Name:           carbons
Version:        0.2.2
Release:        1%{?dist}
Summary:        Experimental XEP0280 plugin for libpurple

License:        GPLv3+
URL:            https://github.com/gkdr/carbons/archive/v%{version}.tar.gz
Source0:	%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  pkgconfig(purple)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(libxml-2.0)

%description
Experimental XEP0280 plugin for libpurple


%prep
%autosetup -p1 -n carbons-%{version}

%build
make %{?_smp_mflags}

%install
%make_install

%files
%{_libdir}/purple-2/*
%doc README.md

%changelog
* Fri Jun 26 2020 Adrian Campos <adriancampos@teachelp.com> - 0.1.3-1
- Initial packaging

